"use strict";
const { reject } = require("async");
const nodemailer = require("nodemailer");
const mailjet = require('node-mailjet');
const Mailgun = require('mailgun-js');
const { resolve } = require("path");
function Email(config){
    const smtpConfig = {
        pool: true,
        host: config.smtp.host,
        port: config.smtp.port,
        secure: config.smtp.secure, // use SSL
        auth: {
            user: config.smtp.username,
            pass: config.smtp.password
        }
    };
    
    this.send = async function(options){
        return new Promise(function (resolve, reject){
            let mailOptions = {
                from: (options.fromName || config.smtp.fromname) + " <" + (options.replyTo || options.sender_email || config.smtp.useremail) + ">", // sender address
                to: options.toList,
                subject: options.subject || "(UNKNOWN)", // Subject line
                text: options.text || "",
                html: options.html || "",
                replyTo : options.replyTo || options.sender_email || (options.fromName || config.smtp.fromname)
            };
    
            switch (config.provider) {
                case 'sendGrid':
                    let transporter = nodemailer.createTransport(smtpConfig);
                    if (options.attachment) mailOptions.attachments = options.attachment;
                    const unique_args = {
                        c__node_env: config.NODE_ENV,
                        c__db_code: entityCode || config.db_name,
                        c__app_code: config.app_code,
                        c__app_mode: config.app_mode,
                        c__sent_mail_id: insRes._id,
                        c_webhook_url: config.smtp.sendgrid_webhook_url || ''
                    }
                    mailOptions.headers = {
                        'X-SMTPAPI': JSON.stringify({ unique_args })
                    }
                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(info);
                        }
                    });
                    break;
                case 'mailJet':
                    mailjet.connect(MJ_APIKEY_PUBLIC, MJ_APIKEY_PRIVATE);
                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(info);
                        }
                    });
                    break;  
                case 'mailgun':
                    let mailgun = Mailgun({ apiKey: config.smtp.mailgun_apikey, domain: config.smtp.mailgun_domain });
                    if (options.attachment) mailOptions.attachment = options.attachment;
                    mailOptions['o:tag'] = [config.environment, config.app_code];
                    mailOptions['o:dkim'] = 'yes';
                    mailOptions['o:tracking'] = 'yes';
                    mailOptions['o:tracking-clicks'] = 'yes';
                    mailOptions['o:tracking-opens'] = 'yes';
                    mailOptions['v:app-vars'] = {
                        c__node_env: config.NODE_ENV,
                        c__db_code: entityCode || config.db_name,
                        c__app_code: config.app_code,
                        c__app_mode: config.app_mode,
                        c__sent_mail_id: insRes._id + "",
                        c_webhook_url: config.smtp.mailgun_webhook_url || ''
                    }
                    mailgun.messages().send(mailOptions, (error, body) => {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(info);
                        }
                    })
                    break;  
                default:
                case 'custom':
                    let transporter = nodemailer.createTransport(smtpConfig);
                    if (options.attachment) mailOptions.attachments = options.attachment;
                    transporter.sendMail(mailOptions, function (error, info) {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(info);
                        }
                    });
                    break;
            }
        })
    }
    
}